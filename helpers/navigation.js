var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.navigation = function (context, options) {
    var ret = '';
    var i = 0;
    var level1 = [];
    var exclude = ['Attractions', 'Footer links','Owners Area'];
    var included = ['Home', 'Surrounding Area', 'Accommodation','No Menu Presence'];
    if (context.length == 0) {
        return ''
    }
    //Standard flat pages
    level1.push({'title': 'Home', 'links': [{'title': 'Home', 'link': '/'}], 'order': 1});
    level1.push({
        'title': 'Surrounding Area',
        'links': [{'title': 'Surrounding Area', 'link': '/local-attractions'}],
        'order': 6
    })
    level1.push({
        'title': 'Park Lifestyle',
        'links': [{'title': 'Park Lifestyle', 'link': '/park-lifestyle'}],
        'order': 5
    });
    level1.push({
        'title': 'Leisure Club',
        'links': [{'title': 'Leisure Club', 'link': '/leisure-club'}],
        'order': 4
    });
    level1.push({
        'title': 'Homes To Own',
        'links': [{'title': 'Homes To Own', 'link': '/homes-to-own'}],
        'order': 3
    });
    level1.push({'title': 'Accommodation', 'links': [{'title': 'Accommodation', 'link': '/accommodation'}], 'order': 2});
    level1.push({'title': 'Contact Us', 'links': [{'title': 'Contact Us', 'link': '/contact-us'}], 'order': 6});
    for (i = 0; i < context.length; i++) {
        if(context[i].url) {
            context[i].order = i + 6;
            if (exclude.indexOf(context[i].level1) == -1 && included.indexOf(context[i].level1) == -1) {
                level1.push({
                    'title': context[i].level1,
                    'links': [{'title': context[i].title, 'link': '/' + context[i].permalink}],
                    'order': context[i].order
                });
                included.push(context[i].level1);
            } else if (included.indexOf(context[i].level1) >= 0) {
                for (var k = 0; k < level1.length; k++) {
                    if (level1[k].title === context[i].level1) {
                        level1[k].links.push({'title': context[i].title, 'link': context[i].permalink})
                    }
                }
            }
        }
    }

    level1.sort(function (obj1, obj2) {
        // Ascending: first age less than the previous
        return obj1.order - obj2.order;
    });
    ret = buildBox(level1);
    return ret;

}

function buildBox(_data) {
    if (_data.length === 0) {
        return ''
    }
    var ret = '';
    //Loop over
    ret += '<ul id="navbar" class="nav navbar-nav navbar-default navbar-static-top corp-navbar">'
    for (var i = 0; i < _data.length; i++) {
        if (_data[i].links.length == 1) {
            ret += '<li><a href="' + _data[i].links[0].link + '">' + _data[i].title + '</a></li>'
        } else {
            ret += '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' + _data[i].title + ' <span class="caret"></span></a><ul class="dropdown-menu">'
            for (var t = 0; t < _data[i].links.length; t++) {
                ret += '<li><a href="' + _data[i].links[t].link + '">' + _data[i].links[t].title + '</a></li>'
            }
            ret += '</ul></li>'
        }
    }
    ret += '<li class="visible-xs"><a href="/owners-area">Owners Area</a></li></ul>';
    return ret

}



