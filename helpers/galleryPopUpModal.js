var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.gallery = function (context, options) {
    logger.debug(context.template);
    var ret = "<div id=\"myModal\" class=\"modal\">" +
        "            <div class=\"modal-content\"><span class=\"close cursor\" onclick=\"closeModal()\">&times;</span>";
    var _tmpLabel= '';
    if (context.length >= 2) {
        ret = ret + "";

        for (var i = 0; i < context.length; i++) {
            if(context[i].label){
                _tmpLabel= context[i].label;
            }else{
                _tmpLabel= '';
            }
            ret = ret + "<div class=\"mySlides\"><div class=\"numbertext\">" + (i+1) +" / "+ context.length +"</div>";
            if(context[i].media.indexOf('.mp4') > 0){
                ret = ret + "<video width=\"100%\" controls><source src=\"" + context[i].media + "\" type=\"video/mp4\">Your browser does not support HTML5 video.</video>";
            }else{

                    ret = ret + "<img style=\"max-width: 100%; max-height: 850px\" class=\"demo\" alt=\""+ _tmpLabel+"\"  src=\"" + optimsieImage(context[i].media,',w_auto') + "\">";

            }

            ret = ret + "</div>"
        }
        ret = ret + "<!-- Next and previous buttons -->\n" +
            "  <a class=\"prev hidden-mobile\" onclick=\"plusSlides(-1)\">&#10094;</a>\n" +
            "  <a class=\"next hidden-mobile\" onclick=\"plusSlides(1)\">&#10095;</a>\n" +
            "\n" +
            "  <!-- Image text -->\n" +
            "  <div class=\"caption-container\">\n" +
            "    <p id=\"caption\"></p>\n" +
            "    <p id=\"swipe-test\" class=\"show-mobile text-white\"> &#8592; Swipe for more &rarr;</p>\n" +
            "  </div>";
        ret = ret + "</div>";
    }

    return ret;
};


function optimsieImage(_url, _width) {
    if (_url.indexOf('c_scale,w_auto,dpr_auto,q_auto') === -1 && _url.indexOf('/upload/') === -1) {
        return _url;
    } else {
        let _tmp = _url.split('/upload/');
        let _tmp2 = _tmp[1].substring(_tmp[1].indexOf('/'));
        return _tmp[0] + '/upload/c_scale,w_auto,dpr_auto,q_auto'+_width + _tmp2;
    }
}


