var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.gallery = function (context, options) {
    logger.debug(context.template);
    var ret = "<header>";
    if (context.length >= 2) {
        ret = ret + "<div class=\"owl-carousel\">";
        for (var i = 0; i < context.length; i++) {

            ret = ret + "<div class=\"item\">";
            if(context[i].media.indexOf('.mp4') > 0){
                ret = ret + "<video controls><source src=\"" + context[i].media + "\" type=\"video/mp4\">Your browser does not support HTML5 video.</video>";
            }else{
                ret = ret + "<img style=\"object-position: " + context[i].left + "% " + context[i].top + "%\" src=\"" + optimsieImage(context[i].media) + "\">";
                if (context[i].label !== '' && context[i].label !== null) {
                    ret = ret + "<div class=\"custom_overlay\"><span class=\"custom_overlay_inner\"><h2>" + context[i].label + "</h2>"
                    if (context[i].link !== '') {
                        ret = ret + "<a href=\"" + context[i].link + "\" class=\"page-scroll btn btn-xl overlay_button\">Tell Me More</a>"
                    }
                    ret = ret + "</span></div>"
                }
            }

            ret = ret + "</div>"
        }
        ret = ret + "</div></header>";
    } else {
        ret = ret + "<div>";
        ret = ret + "<div class=\"item\"><img style=\"object-position: " + context[0].left + " " + context[0].top + "\" class=\"solo-carousel-image\" src=\"" + optimsieImage(context[0].media)
            + "\">";
        if (context[0].label !== '') {
            ret = ret + "<div class=\"custom_overlay\"><span class=\"custom_overlay_inner\"><h2>" + context[0].label + "</h2>"
            if (context[0].link !== '') {
                ret = ret + "<a href=\"" + context[0].link + "\" class=\"page-scroll btn btn-primary overlay_button\">Tell Me More</a>"
            }
            ret = ret + "</span></div>"
        }
        ret = ret + "</div></header>";
    }
    return ret;
};


function optimsieImage(_url) {
    if (_url.indexOf('c_scale,w_auto,dpr_auto,q_auto:low') === -1 && _url.indexOf('/upload/') === -1) {
        return _url;
    } else {
        let _tmp = _url.split('/upload/');
        let _tmp2 = _tmp[1].substring(_tmp[1].indexOf('/'));
        return _tmp[0] + '/upload/c_scale,w_auto,dpr_auto,q_auto:low' + _tmp2;
    }

}

