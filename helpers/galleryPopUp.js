var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.gallery = function (context, options) {
    logger.debug(context.template);
    var ret = "";
    if (context.length >= 2) {
        ret = ret + "<div class=\"row\">";
        for (var i = 0; i < context.length; i++) {

            ret = ret + "<div class=\"column\">";
            if(context[i].media.indexOf('.mp4') > 0){
                ret = ret + "<video style=\"max-height: 100px;padding-bottom: 15px\" controls><source src=\"" + context[i].media + "\" type=\"video/mp4\">Your browser does not support HTML5 video.</video>";
            }else{
                if(i===0){
                    ret = ret + "<img style=\"width: 100%; padding-bottom: 15px; max-height: 40em\" onclick=\"openModal();currentSlide(1)\" src=\"" + optimsieImage(context[i].media,',w_700') + "\" class=\"hover-shadow cursor\">";
                }else{
                    ret = ret + "<img style=\"max-height: 100px;padding-bottom: 15px\" onclick=\"openModal();currentSlide("+(i+1)+")\" src=\"" + optimsieImage(context[i].media,',w_100') + "\" class=\"hover-shadow cursor\">";

                }
            }

            ret = ret + "</div>"
        }
        ret = ret + "</div>";
    } else {
        ret = ret + "<div>";
        ret = ret + "<div class=\"item\"><img style=\"object-position: " + context[0].left + " " + context[0].top + "\" class=\"solo-carousel-image\" src=\"" + optimsieImage(context[0].media)
            + "\">";
        if (context[0].label !== '') {
            ret = ret + "<div class=\"custom_overlay\"><span class=\"custom_overlay_inner\"><h2>" + context[0].label + "</h2>"
            if (context[0].link !== '') {
                ret = ret + "<a href=\"" + context[0].link + "\" class=\"page-scroll btn btn-primary overlay_button\">Tell Me More</a>"
            }
            ret = ret + "</span></div>"
        }
        ret = ret + "</div>";
    }
    return ret;
};


function optimsieImage(_url, _width) {
    if (_url.indexOf('c_scale,w_auto,dpr_auto,q_auto') === -1 && _url.indexOf('/upload/') === -1) {
        return _url;
    } else {
        let _tmp = _url.split('/upload/');
        let _tmp2 = _tmp[1].substring(_tmp[1].indexOf('/'));
        return _tmp[0] + '/upload/c_scale,w_auto,q_auto,dpr_auto'+_width + _tmp2;
    }
}

