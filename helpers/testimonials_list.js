var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.testimonials = function (context, options) {

    logger.debug(context.template);
    let ret = '<div class="col-lg-8 col-lg-offset-2 col-sm-12 text-center"><ul>';
    for (var i = 0; i < context.length; i++) {
        ret = ret + '<li><blockquote>';
        ret = ret + '<p>' + context[i].body + '</p>';
        if (context[i].customer_name !== '') {
            ret = ret + '<h4><i>' + context[i].customer_name + '</i></h4>'
        }
        ret = ret + '</blockquote></li><hr />'
    }
    ret = ret + '</ul></div>';
    if (context.length) {
        return ret;
    } else {
        return '';
    }
}





