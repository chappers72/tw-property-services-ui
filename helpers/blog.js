var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.blogs = function (context, options) {
    logger.debug(context.template);
    var ret = '<div class="row">';
        for (var i = 0; i < context.length; i++) {
            ret = ret + '<a href="/blogs/' + context[i].url + '"><div class="col-md-4"><div class="ot-portfolio-item"><figure class="effect-bubba">';
            ret = ret + '<img src="' + optimsieImage(context[i].media[0].media) + '" class="img-responsive">';
            if (context[i].label !== '') {
                ret = ret + '<figcaption><h2>' + context[i].title + '</h2></figcaption>'
                ret = ret + '</figcaption>'
            }
            ret = ret + '</figure></div></div></a>'
        }
        ret = ret + '</div>';
    return ret;
};


function optimsieImage(_url) {
    if (_url.indexOf('c_scale,w_auto,dpr_auto') === -1 && _url.indexOf('/upload/') === -1) {
        return _url;
    } else {
        let _tmp = _url.split('/upload/');
        let _tmp2 = _tmp[1].substring(_tmp[1].indexOf('/'));
        return _tmp[0] + '/upload/c_scale,w_auto,dpr_auto' + _tmp2;
    }

}
