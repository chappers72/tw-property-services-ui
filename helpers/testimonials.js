var logger = require('../logger');

/**
 * Created by Mark on 04/09/2014.
 */
module.exports.testimonials = function (context, options) {

    logger.debug(context.template);
    let ret = '<div class="section-title text-center p-t-15px"><h2>Testimonials</h2></div><div class="owl-carousel text-center">';
    for (var i = 0; i < context.length; i++) {
        ret = ret + '<div class="item">';
        ret = ret + '<p>' + context[i].body + '</p>';
        if (context[i].customer_name !== '') {
            ret = ret + '<h4><i>' + context[i].customer_name + '</i></h4>'
        }
        ret = ret + '</div>'
    }
    ret = ret + '</div>';
    if (context.length) {
        return ret;
    } else {
        return '';
    }
}





