/**
 * Created by Mark on 01/08/2014.
 * User - login / logout / create new user, edit user, delete user
 */

//var ctrl = require('../controllers/api');

var logger=require('../logger');
var controller=require('../controllers/main');
var authorised=require('../authorised');

module.exports = function(app,passport){

    /*********************USER LOGIN / LOGOUT / ISLOGGED IN / CURRENT USER DETAILS*******************/

        //A simple route to determine if the user is logged in - use this on any page you want to protect but doesnt call any of the APIs below
    app.get('/admin',
        // authorised.ensureAuthorised,
        controller.admin
    );

};
