module.exports = function(app,passport){
    require('./admin')(app, passport); //Front views
    require('./main')(app); //Front views
    require('./user')(app, passport); //Front views
    require('./api/index.js')(app); //Admin routes
};
