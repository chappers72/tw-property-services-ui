
var controller=require('../../controllers/api/siteconfig');
var authorised=require('../../authorised');
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
module.exports = function(app){

    app.post('/api/siteconfig',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.newSiteconfig
    );

    app.put('/api/siteconfig/:id',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.updateSiteconfig
    );

    app.get('/api/siteconfig/:id',
        authorised.ensureAuthorised,
        controller.getSiteconfig
    );

    app.get('/api/siteconfigs',
        authorised.ensureAuthorised,
        controller.getSiteconfigs
    );


    app.delete('/api/siteconfig/:id',
        authorised.ensureAuthorised,
        controller.deleteSiteconfig
    );

};
