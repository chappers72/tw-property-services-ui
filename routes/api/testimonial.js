
var controller=require('../../controllers/api/testimonial');
var authorised=require('../../authorised');
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
module.exports = function(app){

    app.post('/api/testimonial',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.newTestimonial
    );

    app.put('/api/testimonial/:id',
        // authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.updateTestimonial
    );

    app.get('/api/testimonial/:id',
        authorised.ensureAuthorised,
        controller.getTestimonial
    );

    app.get('/api/testimonials',
        authorised.ensureAuthorised,
        controller.getTestimonials
    );


    app.delete('/api/testimonial/:id',
        authorised.ensureAuthorised,
        controller.deleteTestimonial
    );

};
