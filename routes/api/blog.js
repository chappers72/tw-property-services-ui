
var controller=require('../../controllers/api/blog')
var authorised=require('../../authorised');
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
module.exports = function(app){

    app.post('/api/blog',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.newBlog
    );

    app.put('/api/blog/:id',
        multipartyMiddleware,
        controller.updateBlog
    );

    app.get('/api/blog/:id',
        authorised.ensureAuthorised,
        controller.getBlog
    );

    app.get('/api/blogs',
        authorised.ensureAuthorised,
        controller.getBlogs
    );


    app.delete('/api/blog/:id',
        authorised.ensureAuthorised,
        controller.deleteBlog
    );

};
