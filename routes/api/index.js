module.exports = function(app){


    require('./email')(app);
    require('./testimonial')(app);
    require('./sip')(app);
    require('./blog')(app);
    require('./project')(app);
    require('./gallery')(app);
    require('./siteconfig')(app);

    //require('./main')(app);
    //require('./admin')(app);npm run-script ,

};
