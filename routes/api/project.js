
var controller=require('../../controllers/api/project');
var authorised=require('../../authorised');
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
module.exports = function(app){

    app.post('/api/project',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.newProject
    );

    app.put('/api/project/:id',
        // authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.updateProject
    );

    app.get('/api/project/:id',
        authorised.ensureAuthorised,
        controller.getProject
    );

    app.get('/api/projects',
        authorised.ensureAuthorised,
        controller.getProjects
    );


    app.delete('/api/project/:id',
        authorised.ensureAuthorised,
        controller.deleteProject
    );

};
