
var controller=require('../../controllers/api/gallery');
var authorised=require('../../authorised');
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
module.exports = function(app){

    app.post('/api/gallery',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.newGallery
    );

    app.put('/api/gallery/:id',
        // authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.updateGallery
    );

    app.get('/api/gallery/:id',
        authorised.ensureAuthorised,
        controller.getGallery
    );

    app.get('/api/galleries',
        authorised.ensureAuthorised,
        controller.getGalleries
    );


    app.delete('/api/gallery/:id',
        authorised.ensureAuthorised,
        controller.deleteGallery
    );

};
