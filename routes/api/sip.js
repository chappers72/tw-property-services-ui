
var controller=require('../../controllers/api/sip');
var authorised=require('../../authorised');
multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty(),
module.exports = function(app){
    app.post('/api/sip',
        authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.newSIP
    );
    app.put('/api/sip/:id',
        // authorised.ensureAuthorised,
        multipartyMiddleware,
        controller.updateSIP
    );
    app.get('/api/sip/:id',
        authorised.ensureAuthorised,
        controller.getSIP
    );
    app.get('/api/sip-by-template/:template',
        authorised.ensureAuthorised,
        controller.getSIPByTemplate
    );
    app.get('/api/sips',
        authorised.ensureAuthorised,
        controller.getSIPs
    );
    app.delete('/api/sip/:id',
        authorised.ensureAuthorised,
        controller.deleteSIP
    );
};
