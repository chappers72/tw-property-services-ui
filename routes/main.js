var home_ctrl = require('../controllers/views/home');
var sip_ctrl = require('../controllers/views/sip');
var blog_ctrl = require('../controllers/views/blog');
var project_ctrl = require('../controllers/views/project');
var gallery_ctrl = require('../controllers/views/gallery');
var expressSession = require('express-session');
var cookieParser = require('cookie-parser'); // the session is stored in a cookie, so we use this to parse it
// var sitemap = require('../public/sitemap.xml');
var fs = require('fs');
var xml = require('xml');
//Home Page
module.exports = function (app) {
    app.use(cookieParser());

    app.use(expressSession({secret: 'somesecrettokenhere'}));
    //Unique Pages
    app.get('/sitemap.xml', function(req, res, next){
        res.set('Content-Type', 'text/xml');
        res.send(fs.readFileSync('public/sitemap.xml', {encoding: 'utf-8'}))
    });
    app.get('/robots.txt', function(req, res, next){
        res.set('Content-Type', 'text/plain');
        res.send(fs.readFileSync('public/robots.txt'))
    });
    app.get('/analytics.txt', function(req, res, next){
        res.set('Content-Type', 'text/plain');
        res.send(fs.readFileSync('public/analytics.txt'))
    });
    app.get('/', home_ctrl.home);
    app.get('/contact-us', home_ctrl.home);
    app.get('/about-us', add_opts("About Us", "sip", "about-us"), sip_ctrl.render_view);
    app.get('/projects', add_opts("Projects", "sip", "projects"), sip_ctrl.render_view);
    app.get('/blogs', add_opts("Blogs", "sip", "blogs"), sip_ctrl.render_view);
    app.get('/our-services', add_opts("Our Services", "sip", "our-services"), sip_ctrl.render_view);
    app.get('/testimonials', add_opts("Testimonials", "sip", "testimonials"), sip_ctrl.render_view);
    app.get('/galleries', add_opts("Galleries", "sip", "galleries"), sip_ctrl.render_view);
    app.get('/galleries/:title', add_opts("Galleries", "gallery", null), gallery_ctrl.render_view);
    app.get('/:title', add_opts("", "sip", null), sip_ctrl.render_view);
    app.get('/projects/:title', add_opts("Projects", "project", null), project_ctrl.render_view);
    app.get('/blogs/:title', add_opts("Blogs", "blog", null), blog_ctrl.render_view);
    // app.get('/testimonials', add_opts("Testimonials", "testimonials"), general_ctrl.render_view);
    // app.get('/contact-us', add_opts("Contact Us", "contact-us"), general_ctrl.render_view);
    // app.get('/our-services', add_opts("Services", "services"), general_ctrl.render_services_view);
    // app.get('/projects', add_opts("Projects", "projects"), general_ctrl.render_view);
    // app.get('/projects/:title', add_opts("Projects", "projects"), general_ctrl.render_view);
};

function add_opts(page_title, template, key) {
    return function (req, res, next) {
        if (key) {
            req.page_key = key; //Used for locating SIP documents
        } else {
            req.page_key = req.params.title
        }
        req.page_title = page_title;
        req.template = template;
        next();
    }
}
