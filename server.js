/**
 * Created by Mark on 27/10/2014.
 */
var express = require('express');
var os = require('os');
var ifaces = os.networkInterfaces();
var port = process.env.PORT || 8103;
var cors = require('cors');
var User=require("./models/user");
var logger = require('./logger')
var bodyParser = require('body-parser');
var router = express.Router();
var pjson = require('./package.json');
var path = require('path');
var helmet = require('helmet')
var mongoose = require('mongoose');
var config=require('./config');
var passport = require('passport');


var app = express();
app.use(cors());
app.options('*', cors());
app.use(helmet());
var exphbs = require('express3-handlebars');
Swag = require('swag');

var hbs = exphbs.create({
    defaultLayout: 'main',
    partialsDir: [
        'views/partials/'
    ],
    extname:'.handlebars'

});

Swag.registerHelpers(hbs.handlebars);


//Check to see if we are on prod,dev or local.
var IP;
for (var dev in ifaces) {
    var alias = 0;
    ifaces[dev].forEach(function (details) {
        if (details.family == 'IPv4' && details.internal == false) {
            IP = details.address;
            ++alias;
        }
    });
}

if(IP=='185.157.232.145'){
    process.env.NODE_ENV = 'dev';
}else if(IP=='188.166.157.223'){
    process.env.NODE_ENV = 'prod';
}else{
    process.env.NODE_ENV = 'local';
}

app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/*', function (req, res, next) {
    res.header('X-CMSSERVER', os.hostname());
    res.header('X-CMSVERSION', pjson.version);
    next();
});
// Initialize Passport
app.use(passport.initialize());

var initPassport = require('./passport/init');
initPassport(passport);
var routes=require('./routes')(app, passport);


//Satic File handling
app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req,res){
    res.redirect('/404');
});

app.listen(port);

mongoose.connect(config.mongoConnection.url,function(err){
    if(err)
    {
        logger.error(err + ' ' + config.mongoConnection.url)
    }
}); //connect once




logger.info(pjson.name+' Server Started');
logger.info('running in ' + process.env.NODE_ENV);
logger.info('TW Property UI Server happens on port ' + port);
