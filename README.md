# tw-property-services-ui

## Project setup
```
npm install
```
node server to run

##Deploying to Digital Ocean
1. Zip content into a zip file
2. Log in to Digital Ocean
3. Navigate to tw property services droplet
4. Access console to stop server (ctrl + break)
5. Type cloudcmd into console to open web absed ftp service for DO. Runs on 46.101.87.208 port 8000
6. Upload your zipped file.
7. Delete existing files under var/www/tw-property-services
8. Right click and unpack files
9. NPM install if required
10. Ctrl + c to stop cloudcmd
11. node server to restart server


