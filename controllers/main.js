/* GET 'about us' page */

var logger=require('../logger');

module.exports.home = function (req, res) {
            logger.debug('rendering Home page')
            res.render('home')
};

module.exports.admin = function (req, res) {
            logger.debug('rendering admin page');
            res.locals.layout = '../../views/layouts/admin';
            res.render('admin');
};
