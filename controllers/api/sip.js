var sipData = require("../../data/api/sip");
var logger=require('../../logger');

module.exports.newSIP= function (req, res) {
    logger.debug('New SIP Called');
    sipData.newSIP(req,res)
};

module.exports.updateSIP= function (req, res) {
    logger.debug('Update SIP Called');
    sipData.updateSIP(req,res)
};

module.exports.updateSIPImage= function (req, res) {
    logger.debug('Update SIP Image Called');
    sipData.updateSIPImage(req,res)
};
module.exports.getSIP= function (req, res) {
    logger.debug('Get SIP Called');
    sipData.getSIP(req,res)
};
module.exports.getSIPByTemplate= function (req, res) {
    logger.debug('Get SIP Called');
    sipData.getSIPByTemplate(req,res)
};
module.exports.getSIPs= function (req, res) {
    logger.debug('Get SIPs Called');
    sipData.getSIPs(req,res)
};
module.exports.getSIPsInService= function (req, res) {
    logger.debug('Get SIPs Called');
    sipData.getSIPsInService(req,res)
};
module.exports.deleteSIP= function (req, res) {
    logger.debug('Delete SIP Called');
    sipData.deleteSIP(req,res)
};



