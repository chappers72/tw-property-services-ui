var projectData = require("../../data/api/project");
var logger=require('../../logger');

module.exports.newProject= function (req, res) {
    logger.debug('New Project Called');
    projectData.newProject(req,res)
};

module.exports.updateProject= function (req, res) {
    logger.debug('Update Project Called');
    projectData.updateProject(req,res)
};

module.exports.updateProjectImage= function (req, res) {
    logger.debug('Update Project Image Called');
    projectData.updateProjectImage(req,res)
};
module.exports.getProject= function (req, res) {
    logger.debug('Get Project Called');
    projectData.getProject(req,res)
};
module.exports.getProjects= function (req, res) {
    logger.debug('Get Projects Called');
    projectData.getProjects(req,res)
};
module.exports.getProjectsInService= function (req, res) {
    logger.debug('Get Projects Called');
    projectData.getProjectsInService(req,res)
};
module.exports.deleteProject= function (req, res) {
    logger.debug('Delete Project Called');
    projectData.deleteProject(req,res)
};



