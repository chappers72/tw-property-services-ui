var blogData = require("../../data/api/blog");
var logger=require('../../logger');

module.exports.newBlog= function (req, res) {
    logger.debug('New Blog Called');
    blogData.newBlog(req,res)
};

module.exports.updateBlog= function (req, res) {
    logger.debug('Update Blog Called');
    blogData.updateBlog(req,res)
};

module.exports.updateBlogImage= function (req, res) {
    logger.debug('Update Blog Image Called');
    blogData.updateBlogImage(req,res)
};
module.exports.getBlog= function (req, res) {
    logger.debug('Get Blog Called');
    blogData.getBlog(req,res)
};
module.exports.getBlogs= function (req, res) {
    logger.debug('Get Blogs Called');
    blogData.getBlogs(req,res)
};
module.exports.getBlogsInService= function (req, res) {
    logger.debug('Get Blogs Called');
    blogData.getBlogsInService(req,res)
};
module.exports.deleteBlog= function (req, res) {
    logger.debug('Delete Blog Called');
    blogData.deleteBlog(req,res)
};



