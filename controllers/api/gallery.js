var galleryData = require("../../data/api/gallery");
var logger=require('../../logger');

module.exports.newGallery= function (req, res) {
    logger.debug('New Gallery Called');
    galleryData.newGallery(req,res)
};

module.exports.updateGallery= function (req, res) {
    logger.debug('Update Gallery Called');
    galleryData.updateGallery(req,res)
};

module.exports.updateGalleryImage= function (req, res) {
    logger.debug('Update Gallery Image Called');
    galleryData.updateGalleryImage(req,res)
};
module.exports.getGallery= function (req, res) {
    logger.debug('Get Gallery Called');
    galleryData.getGallery(req,res)
};
module.exports.getGalleries= function (req, res) {
    logger.debug('Get Galleries Called');
    galleryData.getGalleries(req,res)
};
module.exports.getGalleriesInService= function (req, res) {
    logger.debug('Get Galleries Called');
    galleryData.getGalleriesInService(req,res)
};
module.exports.deleteGallery= function (req, res) {
    logger.debug('Delete Gallery Called');
    galleryData.deleteGallery(req,res)
};



