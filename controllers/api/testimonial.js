var testimonialData = require("../../data/api/testimonial");
var logger=require('../../logger');

module.exports.newTestimonial= function (req, res) {
    logger.debug('New Testimonal Called');
    testimonialData.newTestimonial(req,res)
};

module.exports.updateTestimonial= function (req, res) {
    logger.debug('Update Testimonal Called');
    testimonialData.updateTestimonial(req,res)
};
module.exports.getTestimonial= function (req, res) {
    logger.debug('Get Testimonal Called');
    testimonialData.getTestimonial(req,res)
};
module.exports.getTestimonials= function (req, res) {
    logger.debug('Get Testimonals Called');
    testimonialData.getTestimonials(req,res)
};

module.exports.deleteTestimonial= function (req, res) {
    logger.debug('Delete Testimonal Called');
    testimonialData.deleteTestimonial(req,res)
};



