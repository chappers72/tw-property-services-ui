var siteconfigData = require("../../data/api/siteconfig");
var logger=require('../../logger');

module.exports.newSiteconfig= function (req, res) {
    logger.debug('New Testimonal Called');
    siteconfigData.newSiteconfig(req,res)
};

module.exports.updateSiteconfig= function (req, res) {
    logger.debug('Update Testimonal Called');
    siteconfigData.updateSiteconfig(req,res)
};
module.exports.getSiteconfig= function (req, res) {
    logger.debug('Get Testimonal Called');
    siteconfigData.getSiteconfig(req,res)
};
module.exports.getSiteconfigs= function (req, res) {
    logger.debug('Get Testimonals Called');
    siteconfigData.getSiteconfigs(req,res)
};

module.exports.deleteSiteconfig= function (req, res) {
    logger.debug('Delete Testimonal Called');
    siteconfigData.deleteSiteconfig(req,res)
};



