/* GET 'about us' page */

var logger = require('../../logger');

/**GET DATA **/
var testimonalData = require('../../data/testimonials');
var sipData = require('../../data/sip');
var templateData = require('../../data/template');
var galleryData = require("../../data/galleries");


/**GET VIEW HELPERS**/
var testimonialHelper = require("../../helpers/testimonials");
var navigationHelper = require('../../helpers/navigation');  //side services layout
var galleryHelper = require('../../helpers/gallery');  //gallery layout
var matchHelper = require('../../helpers/checkPageTitle');  //side services layout
var galleryDataHelper = require("../../helpers/gallery");
var galleryDataHelperPopUp = require("../../helpers/galleryPopUp");
var galleryDataHelperPopUpModal = require("../../helpers/galleryPopUpModal");
var replaceGTLTHelper = require("../../helpers/replaceGTLT");


module.exports.render_view = function (req, res) {
    logger.debug('Getting Gallery VIEW ' + req.title)
    var testimonal_data = {};
    var showDirectors = false;
    var showGalleries = false;
    var showBody = true;
    var gallery_data = {};


    var loadTestimonalData = function () {
        return testimonalData
            .getTestimonials({status: 'live'})
            .then(function (testimonials) {
                testimonal_data = testimonials;
            })
    },
    loadGalleryData = function () {
        var key = req.page_key;
        return galleryData
            .getGallery(key)
            .then(function (gallery) {
                gallery_data = gallery;
            })
    },
        renderPage = function () {
            res.render(req.template, {
                title: gallery_data.title,
                testimonialData: testimonal_data,
                galleryData: gallery_data.media,
                showDirectors: showDirectors,
                showGalleries: showGalleries,
                galleryData: gallery_data,
                showBody: showBody,

                //Helpers to build the HTML
                helpers: {
                    testimonialList: function (context, options) {
                        return testimonialHelper.testimonials(context, options);
                    },
                    navBar: function (context, options) {
                        return navigationHelper.navigation(context, options);
                    },
                    galleryHTML: function (context, options) {
                        return galleryHelper.gallery(context, options);
                    },
                    matchTitleHelper: function (title, expect, options) {
                        return matchHelper.checkMatch(title, expect, options);
                    },
                    galleryHTML: function (context, options) {
                        return galleryDataHelper.gallery(context, options);
                    },
                    galleryHTMLPopUp: function (context, options) {
                        return galleryDataHelperPopUp.gallery(context, options);
                    },
                    galleryHTMLPopUpModal: function (context, options) {
                        return galleryDataHelperPopUpModal.gallery(context, options);
                    },
                    replaceGTLT: function (context, options) {
                        return replaceGTLTHelper.replaceGTLT(context, options);
                    }
                }
            })
        };

    loadTestimonalData().then(loadGalleryData().then(renderPage)).fail(function (err) {
        logger.error(err);
        res.redirect('/404?err=' + err)
    })

};
