/* GET 'about us' page */

var logger = require('../../logger');

/**GET DATA **/
var testimonalData = require('../../data/testimonials');
var sipData = require('../../data/sip');
var templateData = require('../../data/template');
var projectData = require("../../data/projects");


/**GET VIEW HELPERS**/
var testimonialHelper = require("../../helpers/testimonials");
var navigationHelper = require('../../helpers/navigation');  //side services layout
var galleryHelper = require('../../helpers/gallery');  //gallery layout
var matchHelper = require('../../helpers/checkPageTitle');  //side services layout
var projectDataHelper = require("../../helpers/project");
var replaceGTLTHelper = require("../../helpers/replaceGTLT");
var galleryDataHelperPopUp = require("../../helpers/galleryPopUp");
var galleryDataHelperPopUpModal = require("../../helpers/galleryPopUpModal");

module.exports.render_view = function (req, res) {
    logger.debug('Getting Project VIEW ' + req.title)
    var testimonal_data = {};
    var showDirectors = false;
    var showProjects = false;
    var showBody = true;
    var project_data = {};


    var loadTestimonalData = function () {
        return testimonalData
            .getTestimonials({status: 'live'})
            .then(function (testimonials) {
                testimonal_data = testimonials;
            })
    },
    loadProjectData = function () {
        var key = req.page_key;
        return projectData
            .getProject(key)
            .then(function (project) {
                project_data = project;
            })
    },
        renderPage = function () {
            res.render(req.template, {
                title: project_data.title,
                testimonialData: testimonal_data,
                galleryData: project_data.media,
                showDirectors: showDirectors,
                showProjects: showProjects,
                projectData: project_data,
                showBody: showBody,

                //Helpers to build the HTML
                helpers: {
                    testimonialList: function (context, options) {
                        return testimonialHelper.testimonials(context, options);
                    },
                    navBar: function (context, options) {
                        return navigationHelper.navigation(context, options);
                    },
                    galleryHTML: function (context, options) {
                        return galleryHelper.gallery(context, options);
                    },
                    matchTitleHelper: function (title, expect, options) {
                        return matchHelper.checkMatch(title, expect, options);
                    },
                    projectHTML: function (context, options) {
                        return projectDataHelper.projects(context, options);
                    },
                    galleryHTMLPopUp: function (context, options) {
                        return galleryDataHelperPopUp.gallery(context, options);
                    },
                    galleryHTMLPopUpModal: function (context, options) {
                        return galleryDataHelperPopUpModal.gallery(context, options);
                    },
                    replaceGTLT: function (context, options) {
                        return replaceGTLTHelper.replaceGTLT(context, options);
                    }
                }
            })
        };

    loadTestimonalData().then(loadProjectData().then(renderPage)).fail(function (err) {
        logger.error(err);
        res.redirect('/404?err=' + err)
    })

};
