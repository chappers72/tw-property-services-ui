/* GET 'about us' page */

var logger = require('../../logger');

/**GET DATA **/
var testimonalData = require('../../data/testimonials');
var sipData = require('../../data/sip');
var templateData = require('../../data/template');
var projectData = require("../../data/projects");
var galleryData = require("../../data/galleries");
var blogData = require("../../data/blogs");


/**GET VIEW HELPERS**/
var testimonialHelper = require("../../helpers/testimonials");
var testimonialsListHelper = require("../../helpers/testimonials_list");
var navigationHelper = require('../../helpers/navigation');  //side services layout
var galleryHelper = require('../../helpers/gallery');  //gallery layout
var matchHelper = require('../../helpers/checkPageTitle');  //side services layout
var projectDataHelper = require("../../helpers/project");
var galleryListDataHelper = require("../../helpers/gallery_list");
var blogDataHelper = require("../../helpers/blog");
var replaceGTLTHelper = require("../../helpers/replaceGTLT");


module.exports.render_view = function (req, res) {
    logger.debug('Getting SIP VIEW ' + req.title);
    var testimonal_data = {};
    var sip_data = {};
    var showDirectors = false;
    var showProjects = false;
    var showBlogs = false;
    var showGalleries = false;
    var showTestimonials = true;
    var showBody = true;
    var project_data = {};
    var blog_data = {};
    var gallery_data = {};

    var loadTestimonalData = function () {
            return testimonalData
                .getTestimonials({status: 'live'})
                .then(function (testimonials) {
                    testimonal_data = testimonials;
                    loadProjectData();
                })
        },
        loadProjectData = function () {
            return projectData
                .getProjects({status: 'live'})
                .then(function (projects) {
                    project_data = projects;
                    loadGalleryListData();
                })
        },
        loadGalleryListData = function () {
            return galleryData
                .getGalleries({status: 'live'})
                .then(function (galleries) {
                    gallery_data = galleries;
                    loadBlogData();
                })
        },loadBlogData = function () {
            return blogData
                .getBlogs({status: 'live'})
                .then(function (blogs) {
                    blog_data = blogs;
                    renderPage();
                })
        }, loadSipData = function () {
            var key = req.page_key;
            return sipData
                .getSip(key)
                .then(function (sip) {
                    if (!sip) {
                        sip_data = {'Title': ''} //Prevents a hang
                        sip = {};
                    } else {
                        sip_data = sip;
                    }
                    if (sip.title === 'About Us') {
                        showDirectors = true;
                    }
                    if (sip.title === 'Testimonials') {
                        showTestimonials = false;
                    }
                    if (sip.title === 'Projects') {
                        showProjects = true;
                        showBody = false;
                    }
                    if (sip.title === 'Blogs') {
                        showBlogs = true;
                        showBody = false;
                    }
                    if (req.page_key === 'galleries') {
                        showGalleries = true;
                        showBody = true;
                    }
                    loadTestimonalData();
                })
        },
        renderPage = function () {
            logger.debug('Page Title = ' + sip_data.permalink);
            res.render(req.template, {
                title: sip_data.title,
                sipData: sip_data,
                testimonialData: testimonal_data,
                galleryData: sip_data.media,
                showTestimonials: showTestimonials,
                showDirectors: showDirectors,
                showProjects: showProjects,
                showGalleries: showGalleries,
                showBlogs: showBlogs,
                projectData: project_data,
                blogData: blog_data,
                showBody: showBody,
                galleryListData: gallery_data,

                //Helpers to build the HTML
                helpers: {
                    testimonialList: function (context, options) {
                        return testimonialHelper.testimonials(context, options);
                    },
                    testimonialListUL: function (context, options) {
                        return testimonialsListHelper.testimonials(context, options);
                    },
                    navBar: function (context, options) {
                        return navigationHelper.navigation(context, options);
                    },
                    galleryHTML: function (context, options) {
                        return galleryHelper.gallery(context, options);
                    },
                    matchTitleHelper: function (title, expect, options) {
                        return matchHelper.checkMatch(title, expect, options);
                    },
                    projectHTML: function (context, options) {
                        return projectDataHelper.projects(context, options);
                    },
                    galleryListHTML: function (context, options) {
                        return galleryListDataHelper.galleries(context, options);
                    },
                    blogHTML: function (context, options) {
                        return blogDataHelper.blogs(context, options);
                    },
                    replaceGTLT: function (context, options) {
                        return replaceGTLTHelper.replaceGTLT(context, options);
                    }
                }
            })
        };
    loadSipData();


};
