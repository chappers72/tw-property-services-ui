/* GET 'about us' page */

var logger = require('../../logger');

/**GET DATA **/
var testimonalData = require('../../data/testimonials');
var sipData = require('../../data/sip');
var templateData = require('../../data/template');
var blogData = require("../../data/blogs");


/**GET VIEW HELPERS**/
var testimonialHelper = require("../../helpers/testimonials");
var navigationHelper = require('../../helpers/navigation');  //side services layout
var galleryHelper = require('../../helpers/gallery');  //gallery layout
var matchHelper = require('../../helpers/checkPageTitle');  //side services layout
var blogDataHelper = require("../../helpers/blog");
var replaceGTLTHelper = require("../../helpers/replaceGTLT");


module.exports.render_view = function (req, res) {
    logger.debug('Getting Blog VIEW ' + req.params.title);
    var testimonal_data = {};
    var showDirectors = false;
    var showBlogs = false;
    var showBody = true;
    var blog_data = {};


    var loadTestimonalData = function () {
        return testimonalData
            .getTestimonials({status: 'live'})
            .then(function (testimonials) {
                testimonal_data = testimonials;
            })
    },
    loadBlogData = function () {
        var key = req.page_key;
        return blogData
            .getBlog(key)
            .then(function (blog) {
                blog_data = blog;
            })
    },
        renderPage = function () {
            res.render(req.template, {
                title: blog_data.title,
                testimonialData: testimonal_data,
                galleryData: blog_data.media,
                showDirectors: showDirectors,
                showBlogs: showBlogs,
                blogData: blog_data,
                showBody: showBody,

                //Helpers to build the HTML
                helpers: {
                    testimonialList: function (context, options) {
                        return testimonialHelper.testimonials(context, options);
                    },
                    navBar: function (context, options) {
                        return navigationHelper.navigation(context, options);
                    },
                    galleryHTML: function (context, options) {
                        return galleryHelper.gallery(context, options);
                    },
                    matchTitleHelper: function (title, expect, options) {
                        return matchHelper.checkMatch(title, expect, options);
                    },
                    blogHTML: function (context, options) {
                        return blogDataHelper.blogs(context, options);
                    },
                    replaceGTLT: function (context, options) {
                        return replaceGTLTHelper.replaceGTLT(context, options);
                    }
                }
            })
        };

    loadTestimonalData().then(loadBlogData().then(renderPage)).fail(function (err) {
        logger.error(err);
        res.redirect('/404?err=' + err)
    })

};
