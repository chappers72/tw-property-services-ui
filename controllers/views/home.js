var logger = require('../../logger');

/**GET DATA **/
var testimonalData = require("../../data/testimonials");
var projectData = require("../../data/projects");
var templateData = require("../../data/template");

/**GET VIEW HELPERS**/
var testimonialHelper = require("../../helpers/testimonials");
var projectDataHelper = require("../../helpers/project");
var galleryHelper = require("../../helpers/gallery");  //gallery layout
var replaceGTLTHelper = require("../../helpers/replaceGTLT");


//The Home Page - has a different helper
module.exports.home = function (req, res) {
    var testimonal_data = {};
    var template_data = {};
    var siteconfig_data = {};
    var project_data = {};
    const _template = 'Home';
    const showTestimonials = true;

    var loadTestimonalData = function () {
        return testimonalData
            .getTestimonials({status: 'live'})
            .then(function (testimonials) {
                testimonal_data = testimonials;
            })
    };
    var loadProjectData = function () {
        return projectData
            .getProjects({status: 'live'})
            .then(function (projects) {
                project_data = projects;
            })
    };
    var loadTemplateData = function () {
        return templateData
            .getTemplates({status: 'live', template: _template})
            .then(function (templates) {
                template_data = templates;
            })
    };
    renderPage = function () {
        res.render('home', {
            //Title for page
            title: 'Home',
            testimonialData: testimonal_data,
            sipData: template_data[0],
            galleryData: template_data[0].media,
            projectData: project_data,
            showTestimonials: showTestimonials,


            //Helpers to build the HTML
            helpers: {
                testimonialList: function (context, options) {
                    return testimonialHelper.testimonials(context, options);
                },
                galleryHTML: function (context, options) {
                    return galleryHelper.gallery(context, options);
                },
                projectHTML: function (context, options) {
                    return projectDataHelper.projects(context, options);
                },
                replaceGTLT: function (context, options) {
                    return replaceGTLTHelper.replaceGTLT(context, options);
                }
            }
        })
    };

    loadTestimonalData().then(loadTemplateData().then(loadProjectData).then(renderPage))
};

