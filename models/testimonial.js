/**
 * Created by Mark on 09/09/2014.
 */
var mongoose = require('mongoose');
var URLSlugs = require('mongoose-url-slugs');

var TestimonialSchema=new mongoose.Schema({
    customer_name:String,
    body: String,
    status:{type:String},
    created_by: {type: String},
    media: [],
    created_date: {type: Date, default: Date.now},
    updated_by: {type: String},
    updated_date: {type: Date, default: Date.now}
});

TestimonialSchema.plugin(URLSlugs('customer_name', {field: 'url'}));


module.exports = mongoose.model('Testimonial', TestimonialSchema);
