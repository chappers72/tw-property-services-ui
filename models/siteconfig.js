/**
 * Created by Mark on 09/09/2014.
 */


var mongoose = require('mongoose');
var logger = require('../logger');

var SiteConfigSchema=new mongoose.Schema({
    indexed:{type:Boolean,default:false},
    seo_keywords: String,
    seo_metadata: String,
    site_title: String,
    ga_key: String,
    created_by: {type: String},
    created_date: {type: Date, default: Date.now},
    updated_by: {type: String},
    updated_date: {type: Date, default: Date.now}
});

module.exports = mongoose.model('SiteConfig', SiteConfigSchema);