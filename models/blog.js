/**
 * Created by Mark on 09/09/2014.
 */


var mongoose = require('mongoose');
var URLSlugs = require('mongoose-url-slugs');

var BlogSchema = new mongoose.Schema({
    type: String,
    template: String, //which handlebars template to render
    title: String,
    media: [],
    body: String,
    seo_keywords: String,
    seo_metadata: String,
    status: {type: String},
    gallery: {type: String, default:  'false'},
    url:{type:String}, //Computed when saved based on Section / Sub Section
    created_by: {type: String},
    created_date: {type: Date, default: Date.now},
    updated_by: {type: String},
    updated_date: {type: Date, default: Date.now}
});

BlogSchema.plugin(URLSlugs('title', {field: 'url'}));


module.exports = mongoose.model('Blog', BlogSchema);
