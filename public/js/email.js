/**
 * Created by StevenChapman on 27/06/2016.
 */
function sendEmail(body, sendto, subject, area) {
    $.post("/api/email",
        {
            subject: subject,
            email_body: body,
            sendto: sendto
            // sendto: 'stevenjchapman@gmail.com',
            // cc: 'paulv@machurian.co.uk'
        }).done(function () {
        $('#' + area + 'ContactSuccessMsg').removeClass('hidden');
        $('#' + area + 'ContactSendingMsg').addClass('hidden');
        setTimeout(function () {
            // $('#contact-form')[0].reset();
            $('#' + area + 'ContactSuccessMsg').addClass('hidden');
            $('#' + area + 'footerContactSendingMsg').addClass('hidden');
            $('#' + area + '_contact_name').val('');
            $('#' + area + '_contact_email').val('');
            $('#' + area + '_contact_number').val('');
            $('#' + area + '_contact_message').val('');
            $('#' + area + 'SendButton').removeClass('hidden');
        }, 2000);
    })
        .fail(function (xhr, status, error) {
            $('#' + area + 'ContactErrorMsg').removeClass('hidden');
        });

}

function contactUsEmail(_name, _email, _phone, _message, _area) {
    clearAlerts(_area);
    var _err = false;
    if (_name === '') {
        $('#' + _area + '_contact_name_error').removeClass('hidden');
        _err = true;
    }
    if (validateEmailAddress(_email) === false) {
        $('#' + _area + '_contact_email_error').removeClass('hidden');
        _err = true;
    }
    if (_phone === '') {
        $('#' + _area + '_contact_number_error').removeClass('hidden');
        _err = true;
    }
    if (_message === '') {
        $('#' + _area + '_contact_message_error').removeClass('hidden');
        _err = true;
    }
    if (_err === true) {
        return;
    }
    $('#' + _area + 'ContactSendingMsg').removeClass('hidden');
    $('#' + _area + 'SendButton').addClass('hidden');
    var thebody = '<strong>A new contact us request has been submitted:</strong><br /><br />'
    thebody = thebody + 'Name: <strong>' + _name + '</strong><br /><br />'
    thebody = thebody + 'Number: <strong>' + _phone + '</strong><br /><br />'
    thebody = thebody + 'Email: <strong>' + _email + '</strong><br /><br />'
    thebody = thebody + 'Message: <br /><br />' + _message + '</strong>'
    var subject = 'Contact Us Request from ' + _name;
    var sendto = 'stevenjchapman@gmail.com';
    // var sendto = 'info@sarnfaen.co.uk';
    sendEmail(thebody, sendto, subject, _area);
};

function clearAlerts(_area) {
    $('#' + _area + '_contact_name_error').addClass('hidden');
    $('#' + _area + '_contact_number_error').addClass('hidden');
    $('#' + _area + '_contact_email_error').addClass('hidden');
    $('#' + _area + '_contact_message_error').addClass('hidden');
    $('#successMsg').addClass('hidden');
    $('#errorMsg').addClass('hidden');
    $('#sendingMsg').addClass('hidden');
    $('#sendButton').removeClass('hidden');

}

function validateEmailAddress(_email) {
    var atpos = _email.indexOf("@");
    var dotpos = _email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= _email.length) {
        return false;
    }
}
