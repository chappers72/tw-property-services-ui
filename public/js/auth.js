/**
 * Created by StevenChapman on 27/06/2016.
 */
function login(username, password) {
    $.post("/api/user/login",
        {
            username: username,
            password: password
        }).done(function (_response, _data) {
        localStorage.setItem("member", _response.token);
        $.post("/set-token/"+_response.token);
        window.location.href = "/owners-area";
    })
        .fail(function () {
           $("#loginerr").removeClass("hidden");
        })

}

