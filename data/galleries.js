var Gallery= require("../models/gallery"); //We need the mongoose model for Coach
var logger = require('../logger');
var Q = require('q'); //Its asynchoruns request so we need to actually return a promise - this library does this.


module.exports.getGalleries=function(args){
    logger.debug('Starting to get Gallery Data');

    var deferred = Q.defer();
    try{
        var queryString={};


        //Status Filter?  - do not send the property to get Draft & Live
        if(args && args.hasOwnProperty("status")) {
            queryString['status']=args["status"]
        }
        var query = Gallery.find(queryString).sort({updated_date: 'descending'});
        query.exec(function (err, gallery) {
            if (err) {
                logger.error(err)
                return    deferred.reject(err)
            } else {
                return   deferred.resolve(gallery)
            }
        })}
    catch(err){
        deferred.reject(err);
    }

    return deferred.promise
}
module.exports.getGallery = function (permalink) {
    logger.debug('Starting to get Sip ' + permalink);
    var deferred = Q.defer();
    var queryString = {};

    queryString['status'] = 'live';
    queryString['url'] = permalink;
    var query = Gallery.findOne(queryString);
    query.exec(function (err, gallery) {
        if (err) {
            deferred.reject(err)
        } else {
            logger.debug('Got Gallery Data');
            deferred.resolve(gallery)
        }
    })

    return deferred.promise
}
