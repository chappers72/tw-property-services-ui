var Blog= require("../models/blog"); //We need the mongoose model for Coach
var logger = require('../logger');
var Q = require('q'); //Its asynchoruns request so we need to actually return a promise - this library does this.


module.exports.getBlogs=function(args){
    logger.debug('Starting to get Blog Data');

    var deferred = Q.defer();
    try{
        var queryString={};


        //Status Filter?  - do not send the property to get Draft & Live
        if(args && args.hasOwnProperty("status")) {
            queryString['status']=args["status"]
        }
        var query = Blog.find(queryString).sort({updated_date: 'descending'});
        query.exec(function (err, blog) {
            if (err) {
                logger.error(err)
                return    deferred.reject(err)
            } else {
                return   deferred.resolve(blog)
            }
        })}
    catch(err){
        deferred.reject(err);
    }

    return deferred.promise
}
module.exports.getBlog = function (permalink) {
    logger.debug('Starting to get Blog ' + permalink);
    var deferred = Q.defer();
    var queryString = {};

    queryString['status'] = 'live';
    queryString['url'] = permalink;
    var query = Blog.findOne(queryString);
    query.exec(function (err, blog) {
        if (err) {
            deferred.reject(err)
        } else {
            logger.debug('Got Blog Data');
            deferred.resolve(blog)
        }
    })

    return deferred.promise
}
