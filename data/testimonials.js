var Testimonial= require("../models/testimonial"); //We need the mongoose model for Coach
var logger = require('../logger');
var Q = require('q'); //Its asynchoruns request so we need to actually return a promise - this library does this.


module.exports.getTestimonials=function(args){
    logger.debug('Starting to get Testimonal Data');

    var deferred = Q.defer();
    try{
        var queryString={};


        //Status Filter?  - do not send the property to get Draft & Live
        if(args && args.hasOwnProperty("status")) {
            queryString['status']=args["status"]
        }
        var query = Testimonial.find(queryString).sort({updated_date: 'descending'});
        query.exec(function (err, testimonial) {
            if (err) {
                logger.error(err)
                return    deferred.reject(err)
            } else {
                return   deferred.resolve(testimonial)
            }
        })}
    catch(err){
        deferred.reject(err);
    }

    return deferred.promise
}
