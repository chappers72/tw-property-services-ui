var Project= require("../models/project"); //We need the mongoose model for Coach
var logger = require('../logger');
var Q = require('q'); //Its asynchoruns request so we need to actually return a promise - this library does this.


module.exports.getProjects=function(args){
    logger.debug('Starting to get Project Data');

    var deferred = Q.defer();
    try{
        var queryString={};


        //Status Filter?  - do not send the property to get Draft & Live
        if(args && args.hasOwnProperty("status")) {
            queryString['status']=args["status"]
        }
        var query = Project.find(queryString).sort({updated_date: 'descending'});
        query.exec(function (err, project) {
            if (err) {
                logger.error(err)
                return    deferred.reject(err)
            } else {
                return   deferred.resolve(project)
            }
        })}
    catch(err){
        deferred.reject(err);
    }

    return deferred.promise
}
module.exports.getProject = function (permalink) {
    logger.debug('Starting to get Sip ' + permalink);
    var deferred = Q.defer();
    var queryString = {};

    queryString['status'] = 'live';
    queryString['url'] = permalink;
    var query = Project.findOne(queryString);
    query.exec(function (err, project) {
        if (err) {
            deferred.reject(err)
        } else {
            logger.debug('Got Project Data');
            deferred.resolve(project)
        }
    })

    return deferred.promise
}
