/**
 * Created by Mark on 31/10/2014.
 */
var Project = require("../../models/project"); //We need the mongoose model for Coach
var logger = require('../../logger');
var utils = require('../../mongooseutility');
var authorised = require('../../authorised');
var Q = require('q');
var fs = require('fs-extra');
var path = require("path"); //Enhanced filesystem module
var image_utils = require('../../data/imageutils');
var getSlug = require('speakingurl');

//New Project
module.exports.newProject = function (req, res) {

    logger.debug("Creating new Project " + req.body.title)

    var project = new Project();
    utils.updateDocument(project, Project, req.body);

    //Add URL based on section / subsection
    logger.debug(req.body);
console.log('body', req.body);

    project.save(function (err) {
        if (err) {
            //Throw error
            return res.status(400).send({'error': 'Error trying to create a new Project', 'err': err});
        }
        logger.debug('New Project Created');
        res.json(project)
    })

};


module.exports.updateProjectImages = function (req, res) {
    logger.debug(req.body.id)
    Project.findById(req.params.id, function (err, project) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(project, Project, req.body);

        var today = new Date();
        project.updated_by = req.user.username;
        project.updated_date = today;

        project.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({
                    'error': 'Error trying to save a Project ',
                    'err': err
                });
            }
            logger.debug('Project Updated ');
        })
    })
};
module.exports.updateProject = function (req, res) {
    logger.debug(req.body.id)
    Project.findById(req.params.id, function (err, project) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(project, Project, req.body);

        var today = new Date();
        project.updated_date = today;

        project.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to update Project', 'err': err});
            }
            logger.info('Project Updated');
            res.json(project)
        })
    })
};

module.exports.getProject = function (req, res) {
    var query = Project.findById(req.params.id);
    query.exec(function (err, project) {
        if (err) {
            return res.status(400).send({'error': 'error trying to find Project by ID ' + req.params.id, 'err': err});

        }
        if (!project) {
            return res.status(400).send({'error': 'No project found using ' + req.params.id});
        }
        res.json(project);
    })

};

//All ProjectS
module.exports.getProjects = function (req, res) {
    var query = Project.find({});
    //If not an admin we can only return clubs for that client id

    query.sort('-title')
    query.exec(function (err, projects) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all Projects', 'err': err});
        }
        if (!projects) {
            return res.status(400).send({'error': 'No ProjectS Found'});
        }
        res.json(projects)
    })
}

//All ProjectS in management_service
module.exports.getProjectsInService = function (req, res) {
    var service = req.params.service;
    var query = Project.find({'service': service});

    query.sort('-name')
    query.exec(function (err, projects) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all projects by service ' + service, 'err': err});
        }
        if (!projects) {
            return res.status(400).send({'error': 'No projects Found for service ' + service});
        }
        res.json(projects)
    })
}

//Delete a specific client
module.exports.deleteProject = function (req, res) {

    Project.findOne({'_id': req.params.id}, function (err, project) {

        if (err) {
            return res.status(400).send(err);
        }

        project.remove(
            function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    logger.debug("Project deleted");
                    res.json({'success': 'true'})
                }
            }
        );
    });

}





