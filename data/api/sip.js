/**
 * Created by Mark on 31/10/2014.
 */
var SIP = require("../../models/sip"); //We need the mongoose model for Coach
var logger = require('../../logger');
var utils = require('../../mongooseutility');
var authorised = require('../../authorised');
var Q = require('q');
var fs = require('fs-extra');
var path = require("path"); //Enhanced filesystem module
// var image_utils = require('../../data/imageutils')
var getSlug = require('speakingurl');

//New SIP
module.exports.newSIP = function (req, res) {

    logger.debug("Creating new SIP " + req.body.title);
    var sip = new SIP();
    utils.updateDocument(sip, SIP, req.body);
    //Add URL based on section / subsection
    var url = getSlug(req.body.template);
    if (req.body.template === 'Other') {
        url = getSlug(req.body.title);
    }
    sip.url = url;
    sip.save(function (err) {
        if (err) {
            //Throw error
            return res.status(400).send({'error': 'Error trying to create a new SIP', 'err': err});
        }
        logger.debug('New SIP Created');
        res.json(sip)
    })

};


module.exports.updateSIPImages = function (req, res) {
    logger.debug(req.body.id)
    SIP.findById(req.params.id, function (err, sip) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(sip, SIP, req.body);

        var today = new Date();
        sip.updated_by = req.user.username;
        sip.updated_date = today;

        sip.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({
                    'error': 'Error trying to save a SIP ',
                    'err': err
                });
            }
            logger.debug('SIP Updated ');
        })
    })
};
module.exports.updateSIP = function (req, res) {
    logger.debug(req.body.id)
    SIP.findById(req.params.id, function (err, sip) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }
        utils.updateDocument(sip, SIP, req.body);
        var today = new Date();
        sip.updated_date = today;
        sip.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to update SIP', 'err': err});
            }
            logger.info('SIP Updated');
            res.json(sip)
        })
    })
};

module.exports.getSIP = function (req, res) {
    var query = SIP.findById(req.params.id);
    query.exec(function (err, sip) {
        if (err) {
            return res.status(400).send({'error': 'error trying to find SIP by ID ' + req.params.id, 'err': err});

        }
        if (!sip) {
            return res.status(400).send({'error': 'No sip found using ' + req.params.id});
        }
        res.json(sip);
    })

};

module.exports.getSIPByTemplate = function (req, res) {
    var query = SIP.find({'template': req.params.template});
    query.exec(function (err, sip) {
        if (err) {
            return res.status(400).send({
                'error': 'error trying to find SIP by template ' + req.params.template,
                'err': err
            });

        }
        if (!sip) {
            return res.status(400).send({'error': 'No sip found using ' + req.params.template});
        }
        res.json(sip);
    })

};

//All SIPS
module.exports.getSIPs = function (req, res) {
    var query = SIP.find({})
    //If not an admin we can only return clubs for that client id

    query.sort('-title')
    query.exec(function (err, sips) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all SIPs', 'err': err});
        }
        if (!sips) {
            return res.status(400).send({'error': 'No SIPS Found'});
        }
        res.json(sips)
    })
}

//All SIPS in management_service
module.exports.getSIPsInService = function (req, res) {
    var service = req.params.service;
    var query = SIP.find({'service': service});

    query.sort('-name')
    query.exec(function (err, sips) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all sips by service ' + service, 'err': err});
        }
        if (!sips) {
            return res.status(400).send({'error': 'No sips Found for service ' + service});
        }
        res.json(sips)
    })
}

//Delete a specific client
module.exports.deleteSIP = function (req, res) {

    SIP.findOne({'_id': req.params.id}, function (err, sip) {

        if (err) {
            return res.status(400).send(err);
        }

        sip.remove(
            function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    logger.debug("SIP deleted");
                    res.json({'success': 'true'})
                }
            }
        );
    });

}





