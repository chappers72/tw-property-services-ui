/**
 * Created by Mark on 31/10/2014.
 */
var Blog = require("../../models/blog"); //We need the mongoose model for Coach
var logger = require('../../logger');
var utils = require('../../mongooseutility');
var authorised = require('../../authorised');
var Q = require('q');
var fs = require('fs-extra');
var path = require("path"); //Enhanced filesystem module
var image_utils = require('../../data/imageutils');
var getSlug = require('speakingurl');

//New Blog
module.exports.newBlog = function (req, res) {

    logger.debug("Creating new Blog " + req.body.title)

    var blog = new Blog();
    utils.updateDocument(blog, Blog, req.body);

    //Add URL based on section / subsection
    logger.debug(req.body.level1);
    var url = getSlug(req.body.level1);
    logger.debug(url);
    if (req.body.level2) {
        url += '/' + getSlug(req.body.level2);
    }
    blog.url = url;

    gotFiles = false;
    if (req.files) {
        if (req.files.file) {
            gotFiles = true;
        }
    }
    blog.save(function (err) {
        if (err) {
            //Throw error
            return res.status(400).send({'error': 'Error trying to create a new Blog', 'err': err});
        }
        logger.debug('New Blog Created');
        res.json(blog)
    })

};


module.exports.updateBlogImages = function (req, res) {
    logger.debug(req.body.id)
    Blog.findById(req.params.id, function (err, blog) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(blog, Blog, req.body);

        var today = new Date();
        blog.updated_by = req.user.username;
        blog.updated_date = today;

        blog.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({
                    'error': 'Error trying to save a Blog ',
                    'err': err
                });
            }
            logger.debug('Blog Updated ');
        })
    })
};
module.exports.updateBlog = function (req, res) {
    logger.debug(req.body.id)
    Blog.findById(req.params.id, function (err, blog) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(blog, Blog, req.body);

        var today = new Date();
        blog.updated_date = today;

        blog.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to update Blog', 'err': err});
            }
            logger.info('Blog Updated');
            res.json(blog)
        })
    })
};

module.exports.getBlog = function (req, res) {
    var query = Blog.findById(req.params.id);
    query.exec(function (err, blog) {
        if (err) {
            return res.status(400).send({'error': 'error trying to find Blog by ID ' + req.params.id, 'err': err});

        }
        if (!blog) {
            return res.status(400).send({'error': 'No blog found using ' + req.params.id});
        }
        res.json(blog);
    })

};

//All BlogS
module.exports.getBlogs = function (req, res) {
    var query = Blog.find({});
    //If not an admin we can only return clubs for that client id

    query.sort('-title')
    query.exec(function (err, blogs) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all Blogs', 'err': err});
        }
        if (!blogs) {
            return res.status(400).send({'error': 'No BlogS Found'});
        }
        res.json(blogs)
    })
}

//All BlogS in management_service
module.exports.getBlogsInService = function (req, res) {
    var service = req.params.service;
    var query = Blog.find({'service': service});

    query.sort('-name')
    query.exec(function (err, blogs) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all blogs by service ' + service, 'err': err});
        }
        if (!blogs) {
            return res.status(400).send({'error': 'No blogs Found for service ' + service});
        }
        res.json(blogs)
    })
}

//Delete a specific client
module.exports.deleteBlog = function (req, res) {

    Blog.findOne({'_id': req.params.id}, function (err, blog) {

        if (err) {
            return res.status(400).send(err);
        }

        blog.remove(
            function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    logger.debug("Blog deleted");
                    res.json({'success': 'true'})
                }
            }
        );
    });

}





