/**
 * Created by Mark on 31/10/2014.
 */
var Gallery = require("../../models/gallery"); //We need the mongoose model for Coach
var logger = require('../../logger');
var utils = require('../../mongooseutility');
var authorised = require('../../authorised');
var Q = require('q');
var fs = require('fs-extra');
var path = require("path"); //Enhanced filesystem module
var image_utils = require('../../data/imageutils');
var getSlug = require('speakingurl');

//New Gallery
module.exports.newGallery = function (req, res) {

    logger.debug("Creating new Gallery " + req.body.title)

    var gallery = new Gallery();
    utils.updateDocument(gallery, Gallery, req.body);

    //Add URL based on section / subsection
    logger.debug(req.body);

    gallery.save(function (err) {
        if (err) {
            //Throw error
            return res.status(400).send({'error': 'Error trying to create a new Gallery', 'err': err});
        }
        logger.debug('New Gallery Created');
        res.json(gallery)
    })

};


module.exports.updateGalleryImages = function (req, res) {
    logger.debug(req.body.id)
    Gallery.findById(req.params.id, function (err, gallery) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(gallery, Gallery, req.body);

        var today = new Date();
        gallery.updated_by = req.user.username;
        gallery.updated_date = today;

        gallery.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({
                    'error': 'Error trying to save a Gallery ',
                    'err': err
                });
            }
            logger.debug('Gallery Updated ');
        })
    })
};
module.exports.updateGallery = function (req, res) {
    logger.debug(req.body.id)
    Gallery.findById(req.params.id, function (err, gallery) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        logger.debug(req.body.title)

        utils.updateDocument(gallery, Gallery, req.body);

        var today = new Date();
        gallery.updated_date = today;

        gallery.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to update Gallery', 'err': err});
            }
            logger.info('Gallery Updated');
            res.json(gallery)
        })
    })
};

module.exports.getGallery = function (req, res) {
    var query = Gallery.findById(req.params.id);
    query.exec(function (err, gallery) {
        if (err) {
            return res.status(400).send({'error': 'error trying to find Gallery by ID ' + req.params.id, 'err': err});

        }
        if (!gallery) {
            return res.status(400).send({'error': 'No gallery found using ' + req.params.id});
        }
        res.json(gallery);
    })

};

//All GalleryS
module.exports.getGalleries = function (req, res) {
    var query = Gallery.find({});
    //If not an admin we can only return clubs for that client id

    query.sort('-title')
    query.exec(function (err, galleries) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all Galleries', 'err': err});
        }
        if (!galleries) {
            return res.status(400).send({'error': 'No GalleryS Found'});
        }
        res.json(galleries)
    })
}

//All GalleryS in management_service
module.exports.getGalleriesInService = function (req, res) {
    var service = req.params.service;
    var query = Gallery.find({'service': service});

    query.sort('-name')
    query.exec(function (err, galleries) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all galleries by service ' + service, 'err': err});
        }
        if (!galleries) {
            return res.status(400).send({'error': 'No galleries Found for service ' + service});
        }
        res.json(galleries)
    })
}

//Delete a specific client
module.exports.deleteGallery = function (req, res) {

    Gallery.findOne({'_id': req.params.id}, function (err, gallery) {

        if (err) {
            return res.status(400).send(err);
        }

        gallery.remove(
            function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    logger.debug("Gallery deleted");
                    res.json({'success': 'true'})
                }
            }
        );
    });

}





