/**
 * Created by Mark on 31/10/2014.
 */
var Testimonial= require("../../models/testimonial"); //We need the mongoose model for Coach
var logger = require('../../logger');
var utils = require('../../mongooseutility');
var authorised = require('../../authorised');

module.exports.newTestimonial = function (req, res) {

    var testimonial = new Testimonial();
    utils.updateDocument(testimonial, Testimonial, req.body);
    testimonial.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to create a new testimonial', 'err': err});
            }
            logger.debug('New testimonial Created');
            res.json(testimonial)
        })
};


module.exports.updateTestimonial = function (req, res) {
    Testimonial.findById(req.params.id, function (err, testimonial) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        utils.updateDocument(testimonial, Testimonial, req.body);

        var today = new Date();
        testimonial.updated_date = today;

        testimonial.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to update testimonial', 'err': err});
            }
            logger.debug('testimonial Updated');
            res.json(testimonial)
        })
    })
}

module.exports.getTestimonial = function (req, res) {
    var query = Testimonial.findById(req.params.id);
    query.exec(function (err, testimonial) {
        if (err) {
            return res.status(400).send({'error': 'error trying to find Testimonial by ID ' + req.params.id, 'err': err});

        }
        if (!testimonial) {
            return res.status(400).send({'error': 'No Testimonial found using ' + req.params.id});
        }
        res.json(testimonial);
    })

};


module.exports.getTestimonials = function (req, res) {
    var query = Testimonial.find({})
    //If not an admin we can only return clubs for that client id

    query.sort('-title')
    query.exec(function (err, testimonials) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all testimonials', 'err': err});
        }
        if (!testimonials) {
            return res.status(400).send({'error': 'No testimonials Found'});
        }
        res.json(testimonials)
    })
}


//Delete a specific client
module.exports.deleteTestimonial = function (req, res) {

    Testimonial.findOne({'_id': req.params.id}, function (err, testimonial) {

        if (err) {
            return res.status(400).send(err);
        }

        testimonial.remove(
            function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    logger.debug("Testimonial deleted");
                    res.json({'success': 'true'})
                }
            }
        );
    });

}





