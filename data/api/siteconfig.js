/**
 * Created by Mark on 31/10/2014.
 */
var Siteconfig= require("../../models/siteconfig"); //We need the mongoose model for Coach
var logger = require('../../logger');
var utils = require('../../mongooseutility');
var authorised = require('../../authorised');

module.exports.newSiteconfig = function (req, res) {

    var siteconfig = new Siteconfig();
    utils.updateDocument(siteconfig, Siteconfig, req.body);
    siteconfig.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to create a new siteconfig', 'err': err});
            }
            logger.debug('New siteconfig Created');
            res.json(siteconfig)
        })
};


module.exports.updateSiteconfig = function (req, res) {
    Siteconfig.findById(req.params.id, function (err, siteconfig) {
        // In case of any error, return using the done method
        if (err) {
            res.status(400).send(err);
        }

        utils.updateDocument(siteconfig, Siteconfig, req.body);

        var today = new Date();
        siteconfig.updated_by = req.user.username;
        siteconfig.updated_date = today;

        siteconfig.save(function (err) {
            if (err) {
                //Throw error
                return res.status(400).send({'error': 'Error trying to update siteconfig', 'err': err});
            }
            logger.debug('siteconfig Updated');
            res.json(siteconfig)
        })
    })
}

module.exports.getSiteconfig = function (req, res) {
    var query = Siteconfig.findById(req.params.id);
    query.exec(function (err, siteconfig) {
        if (err) {
            return res.status(400).send({'error': 'error trying to find Siteconfig by ID ' + req.params.id, 'err': err});

        }
        if (!siteconfig) {
            return res.status(400).send({'error': 'No Siteconfig found using ' + req.params.id});
        }
        res.json(siteconfig);
    })

};


module.exports.getSiteconfigs = function (req, res) {
    var query = Siteconfig.find({})
    //If not an admin we can only return clubs for that client id

    query.sort('-title')
    query.exec(function (err, siteconfigs) {
        if (err) {
            logger.error({err: err});
            return res.status(400).send({'error': 'error trying to list all siteconfigs', 'err': err});
        }
        if (!siteconfigs) {
            return res.status(400).send({'error': 'No siteconfigs Found'});
        }
        res.json(siteconfigs)
    })
}


//Delete a specific client
module.exports.deleteSiteconfig = function (req, res) {

    Siteconfig.findOne({'_id': req.params.id}, function (err, siteconfig) {

        if (err) {
            return res.status(400).send(err);
        }

        siteconfig.remove(
            function (err) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    logger.debug("Siteconfig deleted");
                    res.json({'success': 'true'})
                }
            }
        );
    });

}





