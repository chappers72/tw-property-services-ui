/**
 * Created by Mark on 31/10/2014.
 */
var Sip = require("../models/sip"); //We need the mongoose model for Coach
var logger = require('../logger');
var Q = require('q'); //Its asynchoruns request so we need to actually return a promise - this library does this.


module.exports.getSips = function (args) {
    logger.debug('Starting to get Sip Data with a service')
    var deferred = Q.defer();
    var queryString = {};


    //Status Filter?  - do not send the property to get Draft & Live
    if (args.hasOwnProperty("status")) {
        queryString['status'] = args["status"]
    }
    var query = Sip.find(queryString).where("service").ne(null).sort({updated_date: 'descending'});
    query.exec(function (err, sip) {
        if (err) {
            deferred.reject(err)
        } else {
            logger.debug('Got SIP Service Data ' + sip.length);
            deferred.resolve(sip)
        }
    })

    return deferred.promise
}


module.exports.getSip = function (permalink) {
    logger.debug('Starting to get Sip ' + permalink);
    var deferred = Q.defer();
    var queryString = {};
    queryString['status'] = 'live';
    queryString['url'] = permalink;
    var query = Sip.findOne(queryString);
    query.exec(function (err, sip) {
        if (err) {
            deferred.reject(err)
        } else {
            logger.debug('Got SIP Data');
            deferred.resolve(sip)
        }
    });

    return deferred.promise
}

