var Template= require("../models/sip"); //We need the mongoose model for Coach
var logger = require('../logger');
var Q = require('q'); //Its asynchoruns request so we need to actually return a promise - this library does this.


module.exports.getTemplates=function(args){
    logger.debug('Starting to get Template Data');

    var deferred = Q.defer();
    try{
        var queryString={};
        queryString['template'] = args.template;
        //Status Filter?  - do not send the property to get Draft & Live
        if(args && args.hasOwnProperty("status")) {
            queryString['status']=args["status"]
        }
        var query = Template.find(queryString).sort({updated_date: 'descending'});
        query.exec(function (err, template) {
            if (err) {
                logger.error(err)
                return    deferred.reject(err)
            } else {
                return   deferred.resolve(template)
            }
        })}
    catch(err){
        deferred.reject(err);
    }

    return deferred.promise
}
