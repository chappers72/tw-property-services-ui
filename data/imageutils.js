/**
 * Created by markb on 28/02/2016.
 */

var gm = require('gm').subClass({imageMagick: true});
var Q = require('q');
var fs = require('fs-extra');
var path = require('path'); //Enhanced filesystem module
var logger = require('../logger');


exports.saveFiles = function (files, save_dir) {
    var deferred = Q.defer();
    var promises = [];
    for (file in files.file) {
        promises.push(saveFile(files.file[file], save_dir));
    }
    Q.all(promises).then(function (result) {
        deferred.resolve(result)

    }, function (err) {
        logger.error(err);
        deferred.reject(err)
    })

    return deferred.promise;
}


function saveFile(file, save_dir) {
    var deferred = Q.defer()
    logger.debug('save file ' + file.path + ' to ' + path.resolve("./") + '/public/images/' + save_dir + '/' + file.name);
    var dir = path.resolve("./") + '/public/images/' + save_dir;
    fs.ensureDir(dir, function (err) {
        fs.rename(
            file.path,
            dir + file.name,
            function (error) {
                if (error) {
                    deferred.reject(error)
                } else {
                    if (file.name.slice(-4) === '.pdf') {
                        deferred.resolve(file.name)
                    } else {
                        resizeFileThumb(dir, file.name).then(
                            resizeFileSmall(dir, file.name).then(
                                deferred.resolve(file.name)
                            )
                        )
                    }

                }
            }
        );
    })
    return deferred.promise;
}

function resizeFileThumb(original_dir, filename) {
    var deferred = Q.defer();
    var dir = original_dir + '/thumb/';
    fs.ensureDir(dir, function (err) {
        thumbPath = dir + filename;
        gm(original_dir + filename)
            .options({imageMagick: true})
            .autoOrient()
            .resize(200).write(thumbPath, function (err) {
            if (err) logger.info(err);
            deferred.resolve()
        })

    })

    return deferred.promise;
}

function resizeFileSmall(original_dir, filename) {
    var deferred = Q.defer()
    var dir = original_dir + '/small/';
    fs.ensureDir(dir, function (err) {
        thumbPath = dir + filename;
        gm(original_dir + filename)
            .options({imageMagick: true})
            .autoOrient()
            .resize(300).write(thumbPath, function (err) {
            if (err) logger.info(err);
            deferred.resolve()
        })

    })

    return deferred.promise;
}
